<?php
// Variables
$num_cifras = 0;
$cantidad = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $num_cifras = (int)$_POST["txtnumcifras"];

    // Proceso
    $limite_inferior = pow(10, $num_cifras - 1);
    $limite_superior = pow(10, $num_cifras) - 1;

    for($i = $limite_inferior; $i <= $limite_superior; $i++) {
        if(es_capicua($i)) {
            $cantidad++;
        }
    }
}

function es_capicua($num) {
    $str_num = strval($num);
    $len = strlen($str_num);
    for($i = 0; $i < $len / 2; $i++) {
        if($str_num[$i] != $str_num[$len - 1 - $i]) {
            return false;
        }
    }
    return true;
}

?>

<html>
<head>
    <title>Problema 50</title>
    <link rel="stylesheet" href="estilos50.css">
</head>
<body>
    <form method="post" action="ejercicio50.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 50</strong></td>
            </tr>
            <tr>
                <td>Ingrese la cantidad de cifras</td>
                <td>
                    <input name="txtnumcifras" type="text" id="txtnumcifras" value="<?=$num_cifras?>" />
                </td>
            </tr>
            <?php if ($cantidad > 0) { ?>
            <tr>
                <td>Cantidad de números capicúa</td>
                <td>
                    <input name="txtcantidad" type="text" class="TextoFondo" id="txtcantidad" value="<?=$cantidad?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
