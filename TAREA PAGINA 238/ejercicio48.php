<?php
// Variable
$n = 0;
$permutaciones = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $n = (int)$_POST["txtn"];

    // Proceso
    $permutaciones = 1;
    for($i = $n; $i >= 1; $i--) {
        $permutaciones *= $i;
    }
}
?>

<html>
<head>
    <title>Problema 48</title>
    <link rel="stylesheet" href="estilos48.css">
</head>
<body>
    <form method="post" action="ejercicio48.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 48</strong></td>
            </tr>
            <tr>
                <td>Ingrese un numero</td>
                <td>
                    <input name="txtn" type="text" id="txtn" value="<?=$n?>" />
                </td>
            </tr>
            <?php if ($permutaciones > 0) { ?>
            <tr>
                <td>Número de permutaciones posibles</td>
                <td>
                    <input name="txtpermutaciones" type="text" class="TextoFondo" id="txtpermutaciones" value="<?=$permutaciones?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
