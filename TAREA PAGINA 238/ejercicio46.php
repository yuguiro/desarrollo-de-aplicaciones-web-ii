<?php
// Variables
$num_cifras = 0;
$divisor = 0;
$cantidad = 0;

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $num_cifras = (int)$_POST["txtnumcifras"];
    $divisor = (int)$_POST["txtdivisor"];

    // Proceso
    $limite_inferior = pow(10, $num_cifras - 1);
    $limite_superior = pow(10, $num_cifras) - 1;

    for($i = $limite_inferior; $i <= $limite_superior; $i++) {
        if($i % $divisor == 0) {
            $cantidad++;
        }
    }
}

?>

<html>
<head>
    <title>Problema 46</title>
    <link rel="stylesheet" href="estilos46.css">
</head>
<body>
    <form method="post" action="ejercicio46.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 46</strong></td>
            </tr>
            <tr>
                <td>Ingrese la cantidad de cifras</td>
                <td>
                    <input name="txtnumcifras" type="text" id="txtnumcifras" value="<?=$num_cifras?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese el divisor</td>
                <td>
                    <input name="txtdivisor" type="text" id="txtdivisor" value="<?=$divisor?>" />
                </td>
            </tr>
            <?php if ($cantidad > 0) { ?>
            <tr>
                <td>Cantidad de múltiplos</td>
                <td>
                    <input name="txtcantidad" type="text" class="TextoFondo" id="txtcantidad" value="<?=$cantidad?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
