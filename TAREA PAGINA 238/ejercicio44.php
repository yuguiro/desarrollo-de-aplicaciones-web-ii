<?php
// Variables
$n = 0;
$fibo = array();

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $n = (int)$_POST["txtn"];

    // Proceso
    $fibo = array(0, 1);
    $sum = 1;
    while($fibo[count($fibo)-1] + $fibo[count($fibo)-2] < $n) {
        $fibo[] = $fibo[count($fibo)-1] + $fibo[count($fibo)-2];
        $sum += $fibo[count($fibo)-1];
    }
}
?>

<html>
<head>
    <title>Problema 44</title>
    <link rel="stylesheet" href="estilos44.css">
</head>
<body>
    <form method="post" action="ejercicio44.php">
        <table width="241" border="0">
            <tr>
                <td colspan="2"><strong>Problema 44</strong></td>
            </tr>
            <tr>
                <td width="81">Ingrese un número</td>
                <td width="150">
                    <input name="txtn" type="text" id="txtn" value="<?=$n?>" />
                </td>
            </tr>
            <?php if (!empty($fibo)) { ?>
            <tr>
                <td>Números de la serie de fibonacci menores a <?=$n?>:</td>
                <td><?= implode(", ", $fibo) ?></td>
            </tr>
            <tr>
                <td>Suma de los números:</td>
                <td><?=$sum?></td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
