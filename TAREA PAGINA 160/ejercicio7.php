<!DOCTYPE html>
<html>
<head>
    <title>Estado civil</title>
    <style>
        body {
            background-color: #808080;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        .container {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        h1 {
            margin-top: 0;
        }

        form {
            margin-bottom: 20px;
        }

        input[type="submit"] {
            width: 150px;
            height: 40px;
            font-size: 16px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            margin-top: 10px;
        }

        .resultados {
            margin-top: 20px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Estado civil</h1>
    <form method="post">
        <label for="codigo">Código:</label>
        <input type="number" name="codigo" id="codigo" required>
        <br><br>
        <input type="submit" value="Obtener estado civil">
        <div class="resultados">
            <?php
            if(isset($_POST['codigo'])) {
                $codigo = $_POST['codigo'];

                $estado_civil = "";

                switch($codigo) {
                    case 0:
                        $estado_civil = "Soltero";
                        break;
                    case 1:
                        $estado_civil = "Casado";
                        break;
                    case 2:
                        $estado_civil = "Divorciado";
                        break;
                    case 3:
                        $estado_civil = "Viudo";
                        break;
                    default:
                        $estado_civil = "Código inválido";
                }

                echo "El estado civil correspondiente al código $codigo es: <strong>$estado_civil</strong>.";
            }
            ?>
        </div>
    </form>
</div>
</body>
</html>

