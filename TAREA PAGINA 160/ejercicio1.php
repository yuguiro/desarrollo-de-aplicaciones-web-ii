<!DOCTYPE html>
<html>
<head>
    <title>Mes en letras</title>
    <style>
        body {
            background-color: #808080;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        .container {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        h1 {
            margin-top: 0;
        }

        form {
            margin-bottom: 20px;
        }

     input[type="submit"] {
        width: 150px;
        height: 40px;
        font-size: 16px;
        background-color: #4CAF50;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
      }

        .resultados {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>MES EN LETRAS</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="mes">Ingrese el número (1-12):</label>
        <input type="number" name="mes" id="mes" min="1" max="12" required><br><br>
        <input type="submit" value="Convertir">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $mes = $_POST["mes"];
        
        $meses = array(
            1 => "Enero",
            2 => "Febrero",
            3 => "Marzo",
            4 => "Abril",
            5 => "Mayo",
            6 => "Junio",
            7 => "Julio",
            8 => "Agosto",
            9 => "Septiembre",
            10 => "Octubre",
            11 => "Noviembre",
            12 => "Diciembre"
        );

        echo "<div class='resultados'>";
        echo "<h2>Resultado:</h2>";

        if (isset($meses[$mes])) {
            echo "El número $mes corresponde al mes de " . $meses[$mes] . ".";
        } else {
            echo "Número de mes inválido. Por favor, ingrese un número de mes válido.";
        }

        echo "</div>";
    }
    ?>
</div>
</body>
</html>

