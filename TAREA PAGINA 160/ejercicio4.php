<!DOCTYPE html>
<html>
<head>
    <title>Nombre del canal de televisión peruano</title>
    <style>
        body {
            background-color: #808080;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        .container {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        h1 {
            margin-top: 0;
        }

        form {
            margin-bottom: 20px;
        }

        input[type="submit"] {
            width: 150px;
            height: 40px;
            font-size: 16px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            margin-top: 10px;
        }

        .resultados {
            margin-top: 20px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Nombre del canal de televisión peruano</h1>
    <form method="post">
        <label for="canal">Ingresa el número de canal:</label>
        <input type="text" name="canal" required>
        <br>
        <input type="submit" value="Enviar">
        <div class="resultados">
            <?php
            if(isset($_POST['canal'])) {
                $canal = $_POST['canal'];
                $nombreCanal = '';

                switch($canal) {
                    case '47':
                        $nombreCanal = 'América Televisión';
                        break;
                    case '4':
                        $nombreCanal = 'Red TV';
                        break;
                    case '2':
                        $nombreCanal = 'TV Perú';
                        break;
                    case '7':
                        $nombreCanal = 'Frecuencia Latina';
                        break;
                    case '5':
                        $nombreCanal = 'Panamericana Televisión';
                        break;
                    case '21':
                        $nombreCanal = 'ATV';
                        break;
                    default:
                        $nombreCanal = 'Canal desconocido';
                        break;
                }

                echo "El canal número <strong>$canal</strong> es <strong>$nombreCanal</strong>.";
            }
            ?>
        </div>
    </form>
</div>
</body>
</html>
