<!DOCTYPE html>
<html>
<head>
    <title>Día de la semana</title>
    <style>
        body {
            background-color: #808080;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        .container {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        h1 {
            margin-top: 0;
        }

        form {
            margin-bottom: 20px;
        }

        input[type="submit"] {
            width: 150px;
            height: 40px;
            font-size: 16px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        .resultados {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            margin-top: 20px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>DÍA DE LA SEMANA EN NÚMEROS</h1>
    <form method="post">
        <label for="numero">Ingresa un número del 1 al 7:</label>
        <input type="number" name="numero" min="1" max="7" required>
        <input type="submit" value="Convertir">
    </form>
    <?php
    if(isset($_POST['numero'])) {
        $numero = $_POST['numero'];
        switch($numero) {
            case 1:
                echo "<div class='resultados'>El número corresponde al dia: Domingo</div>";
                break;
            case 2:
                echo "<div class='resultados'>El número corresponde al dia: Lunes</div>";
                break;
            case 3:
                echo "<div class='resultados'>El número corresponde al dia: Martes</div>";
                break;
            case 4:
                echo "<div class='resultados'>El número corresponde al dia: Miércoles</div>";
                break;
            case 5:
                echo "<div class='resultados'>El número corresponde al dia: Jueves</div>";
                break;
            case 6:
                echo "<div class='resultados'>El número corresponde al dia: Viernes</div>";
                break;
            case 7:
                echo "<div class='resultados'>El número corresponde al dia: Sábado</div>";
                break;
            default:
                echo "<div class='resultados'>Número inválido</div>";
        }
    }
    ?>
</div>
</body>
</html>

