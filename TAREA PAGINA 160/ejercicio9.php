<?php
// Variables
$sexo = "";
$puntaje = 0;
$ciudad = "";

if(isset($_POST["btnCalcular"])) {
    // Entrada
    $sexo = $_POST["ddlSexo"];
    $puntaje = (int)$_POST["txtPuntaje"];

    // Proceso
    if ($puntaje >= 18 && $puntaje <= 35) {
        if ($sexo == "Masculino") {
            $ciudad = "Arequipa";
        } else if ($sexo == "Femenino") {
            $ciudad = "Cuzco";
        }
    } elseif ($puntaje >= 36 && $puntaje <= 75) {
        if ($sexo == "Masculino") {
            $ciudad = "Cuzco";
        } else if ($sexo == "Femenino") {
            $ciudad = "Iquitos";
        }
    } elseif ($puntaje > 75) {
        if ($sexo == "Masculino") {
            $ciudad = "Iquitos";
        } else if ($sexo == "Femenino") {
            $ciudad = "Arequipa";
        }
    }
}

?>

<html>
<head>
    <title>Problema 29</title>
    <link rel="stylesheet" href="estilos29.css">
</head>
<body>
    <form method="post" action="ejercicio29.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Problema 29</strong></td>
            </tr>
            <tr>
                <td>Sexo</td>
                <td>
                    <select name="ddlSexo">
                        <option value="Masculino">Masculino</option>
                        <option value="Femenino">Femenino</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Puntaje en el examen</td>
                <td>
                    <input name="txtPuntaje" type="text" value="<?=$puntaje?>" />
                </td>
            </tr>
            <?php if ($ciudad != "") { ?>
            <tr>
                <td>Ciudad a visitar</td>
                <td>
                    <input name="txtCiudad" type="text" class="TextoFondo" value="<?=$ciudad?>" />
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" value="CALCULAR" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>