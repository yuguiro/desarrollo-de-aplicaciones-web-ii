<!DOCTYPE html>
<html>
<head>
    <title>Pago de manzanas en frutería</title>
    <style>
        body {
            background-color: #808080;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        .container {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        h1 {
            margin-top: 0;
        }

        form {
            margin-bottom: 20px;
        }

        input[type="submit"] {
            width: 150px;
            height: 40px;
            font-size: 16px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            margin-top: 10px;
        }

        .resultados {
            margin-top: 20px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>FRUTERIA CON DESCUENTO</h1>
    <form method="post">
        <label for="kilos">Cantidad de kilos:</label>
        <input type="number" name="kilos" id="kilos" step="0.01" required>
        <br><br>
        <input type="submit" value="Calcular">
        <div class="resultados">
            <?php
            if(isset($_POST['kilos'])) {
                $kilos = $_POST['kilos'];

                $descuento = 0;
                $precio_por_kilo = 2.50;
                $total_pagar = 0;

                if($kilos <= 2) {
                    $descuento = 0;
                } elseif($kilos <= 5) {
                    $descuento = 0.10;
                } elseif($kilos <= 10) {
                    $descuento = 0.20;
                } else {
                    $descuento = 0.30;
                }

                $total_pagar = $kilos * $precio_por_kilo * (1 - $descuento);

                echo "La cantidad a pagar es: <strong>$total_pagar</strong> soles.";
            }
            ?>
        </div>
    </form>
</div>
</body>
</html>
