<!DOCTYPE html>
<html>
<head>
    <title>Nombre del operador aritmético</title>
    <style>
        body {
            background-color: #808080;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        .container {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        h1 {
            margin-top: 0;
        }

        form {
            margin-bottom: 20px;
        }

        input[type="submit"] {
            width: 150px;
            height: 50px;
            font-size: 16px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        .resultados {
            margin-top: 20px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>NOMBRE DEL OPERADOR ARITMÉTICO</h1>
    <form method="post">
        <label for="operador">Ingresa un operador aritmético (+, -, * o /):</label>
        <input type="text" name="operador" maxlength="1" required>
        <br>
        <input type="submit" value="Enviar">
        <div class="resultados">

            <?php
            if(isset($_POST['operador'])) {
                $operador = $_POST['operador'];
                $nombreOperador = '';

                switch($operador) {
                    case '+':
                        $nombreOperador = 'Suma';
                        break;
                    case '-':
                        $nombreOperador = 'Resta';
                        break;
                    case '*':
                        $nombreOperador = 'Multiplicación';
                        break;
                    case '/':
                        $nombreOperador = 'División';
                        break;
                    default:
                        $nombreOperador = 'Operador inválido';
                        break;
                }

                echo "EL OPERADOR <strong>$operador</strong> SE LLAMA <strong>$nombreOperador</strong>.";
            }
            ?>
        </div>
    </form>
</div>
</body>
</html>

