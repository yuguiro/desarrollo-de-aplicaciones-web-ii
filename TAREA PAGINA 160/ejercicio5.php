<!DOCTYPE html>
<html>
<head>
    <title>Monto del descuento al sueldo</title>
    <style>
        body {
            background-color: #808080;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        .container {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        h1 {
            margin-top: 0;
        }

        form {
            margin-bottom: 20px;
        }

        input[type="submit"] {
            width: 150px;
            height: 40px;
            font-size: 16px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            margin-top: 10px;
        }

        .resultados {
            margin-top: 20px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>MONTO DEL DESCUENTO AL SUELDO</h1>
    <form method="post">
        <label for="tipo">TIPO DE TRABAJADOR:</label>
        <select name="tipo" id="tipo" required>
            <option value="obrero">Obrero</option>
            <option value="empleado">Empleado</option>
        </select>
        <br><br>
        <label for="genero">GÉNERO:</label>
        <select name="genero" id="genero" required>
            <option value="hombre">Hombre</option>
            <option value="mujer">Mujer</option>
        </select>
        <br><br>
        <label for="sueldo">SUELDO:</label>
        <input type="number" name="sueldo" id="sueldo" required>
        <br><br>
        <input type="submit" value="Calcular">
        <div class="resultados">
            <?php
            if(isset($_POST['tipo']) && isset($_POST['genero']) && isset($_POST['sueldo'])) {
                $tipo = $_POST['tipo'];
                $genero = $_POST['genero'];
                $sueldo = $_POST['sueldo'];

                $descuento = 0;

                if($tipo == 'obrero') {
                    if($genero == 'hombre') {
                        $descuento = $sueldo * 0.15;
                    } elseif($genero == 'mujer') {
                        $descuento = $sueldo * 0.10;
                    }
                } elseif($tipo == 'empleado') {
                    if($genero == 'hombre') {
                        $descuento = $sueldo * 0.20;
                    } elseif($genero == 'mujer') {
                        $descuento = $sueldo * 0.15;
                    }
                }

                echo "El monto del descuento al sueldo es: <strong>$descuento</strong>.";
            }
            ?>
        </div>
    </form>
</div>
</body>
</html>
