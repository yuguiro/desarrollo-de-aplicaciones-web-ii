<!DOCTYPE html>
<html>
  <head>
    <title>Calcular área y perímetro de un rectángulo</title>
    <style>
      body {
        background-color: #808080;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        font-family: Arial, sans-serif;
      }
      
      .container {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
      }
      
      h1 {
        margin-top: 0;
        color: #000;
      }
      
      form {
        margin-bottom: 20px;
      }
      
      label {
        display: block;
        margin-bottom: 10px;
        color: #000;
      }
      
      input[type="number"] {
        width: 200px;
        height: 30px;
        font-size: 16px;
        padding: 5px;
        color: #000;
        -moz-appearance: textfield; /* Remover flechas en Firefox */
      }
      
      input[type="number"]::-webkit-outer-spin-button,
      input[type="number"]::-webkit-inner-spin-button {
        -webkit-appearance: none; /* Remover flechas en Chrome, Safari, Opera */
        margin: 0; /* Espacio entre los elementos de la flecha */
      }
      
      input[type="submit"] {
        width: 150px;
        height: 40px;
        font-size: 16px;
        background-color: #4CAF50;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
      }
      
      .resultados {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
        color: #000;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h1>CALCULAR ÁREA Y PERÍMETRO DE UN RECTÁNGULO</h1>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="base">Ingrese la base del rectángulo:</label>
        <input type="number" name="base" id="base" pattern="[0-9]*" inputmode="numeric"><br><br>
        <label for="altura">Ingrese la altura del rectángulo:</label>
        <input type="number" name="altura" id="altura" pattern="[0-9]*" inputmode="numeric"><br><br>
        <input type="submit" value="Calcular">
      </form>
      <?php
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $base = $_POST["base"];
        $altura = $_POST["altura"];
        
        $area = $base * $altura;
        $perimetro = 2 * ($base + $altura);
        
        echo "<div class='resultados'>";
        echo "<h2>Resultados:</h2>";
        echo "El área del rectángulo es " . $area . " unidades cuadradas y el perímetro es " . $perimetro . " unidades.";
        echo "</div>";
      }
      ?>
    </div>
  </body>
</html>

