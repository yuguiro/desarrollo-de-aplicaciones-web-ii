<!DOCTYPE html>
<html>
  <head>
    <title>Convertir horas a minutos y segundos</title>
    <style>
      body {
        background-color: #808080;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        font-family: Arial, sans-serif;
      }
      
      .container {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
      }
      
      h1 {
        margin-top: 0;
        color: #000;
      }
      
      form {
        margin-bottom: 20px;
      }
      
      label {
        display: block;
        margin-bottom: 10px;
        color: #000;
      }
      
      input[type="number"] {
        width: 200px;
        height: 30px;
        font-size: 16px;
        padding: 5px;
        color: #000;
        -moz-appearance: textfield; /* Remover flechas en Firefox */
      }
      
      input[type="number"]::-webkit-outer-spin-button,
      input[type="number"]::-webkit-inner-spin-button {
        -webkit-appearance: none; /* Remover flechas en Chrome, Safari, Opera */
        margin: 0; /* Espacio entre los elementos de la flecha */
      }
      
      input[type="submit"] {
        width: 150px;
        height: 40px;
        font-size: 16px;
        background-color: #4CAF50;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
      }
      
      .resultados {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
        color: #000;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h1>CONVERTIR HORAS A MINUTOS Y SEGUNDOS</h1>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="horas">Ingrese la cantidad de horas:</label>
        <input type="number" name="horas" id="horas" pattern="[0-9]*" inputmode="numeric"><br><br>
        <input type="submit" value="Convertir">
      </form>
      <?php
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $horas = $_POST["horas"];
        
        $minutos = $horas * 60;
        $segundos = $horas * 3600;
        
        echo "<div class='resultados'>";
        echo "<h2>Resultados:</h2>";
        echo $horas . " horas son equivalentes a " . $minutos . " minutos y " . $segundos . " segundos.";
        echo "</div>";
      }
      ?>
    </div>
  </body>
</html>
