<!DOCTYPE html>
<html>
  <head>
    <title>Calcular porcentaje de 4 números enteros</title>
    <style>
      body {
        background-color: #808080;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        font-family: Arial, sans-serif;
        color: #000;
      }
      
      .container {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
      }
      
      h1 {
        margin-top: 0;
        text-transform: uppercase;
      }
      
      form {
        margin-bottom: 20px;
      }
      
      label {
        display: block;
        margin-bottom: 10px;
      }
      
      input[type="number"] {
        width: 200px;
        height: 30px;
        font-size: 16px;
        padding: 5px;
      }
      
      input[type="submit"] {
        width: 150px;
        height: 40px;
        font-size: 16px;
        background-color: #4CAF50;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
      }
      
      .resultados {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
      }
    </style>
  </head>
  <body>
    <div style="background-color: #D3D3D3; padding: 20px; border-radius: 5px; text-align: center;">
      <h1>CALCULAR PORCENTAJE DE 4 NÚMEROS</h1>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="num1">Ingrese el primer número:</label>
        <input type="number" name="num1" id="num1"><br><br>
        <label for="num2">Ingrese el segundo número:</label>
        <input type="number" name="num2" id="num2"><br><br>
        <label for="num3">Ingrese el tercer número:</label>
        <input type="number" name="num3" id="num3"><br><br>
        <label for="num4">Ingrese el cuarto número:</label>
        <input type="number" name="num4" id="num4"><br><br>
        <input type="submit" value="Calcular">
      </form>
      <?php
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        $num3 = $_POST["num3"];
        $num4 = $_POST["num4"];
        
        $suma = $num1 + $num2 + $num3 + $num4;
        
        $porcentaje1 = ($num1 / $suma) * 100;
        $porcentaje2 = ($num2 / $suma) * 100;
        $porcentaje3 = ($num3 / $suma) * 100;
        $porcentaje4 = ($num4 / $suma) * 100;
        
        echo "<div style='background-color: #D3D3D3; padding: 20px; border-radius: 5px; text-align: center;'>";
        echo "<h2>Resultados:</h2>";
        echo "El porcentaje del primer número es: " . round($porcentaje1, 2) . "%<br>";
        echo "El porcentaje del segundo número es: " . round($porcentaje2, 2) . "%<br>";
        echo "El porcentaje del tercer número es: " . round($porcentaje3, 2) . "%<br>";
        echo "El porcentaje del cuarto número es: " . round($porcentaje4, 2) . "%";
        echo "</div>";
      }
      ?>
    </div>
  </body>
</html>