<!DOCTYPE html>
<html>
  <head>
    <title>Convertir milímetros a metros, decímetros, centímetros y milímetros</title>
    <style>
      body {
        background-color: #808080;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        font-family: Arial, sans-serif;
        color: #000; /* Cambio del color de las letras a negro */
      }
      
      .container {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
      }
      
      h1 {
        margin-top: 0;
        text-transform: uppercase;
      }
      
      form {
        margin-bottom: 20px;
      }
      
      label {
        display: block;
        margin-bottom: 10px;
      }
      
      input[type="text"] {
        width: 200px;
        height: 30px;
        font-size: 16px;
        padding: 5px;
      }
      
      input[type="submit"] {
        width: 150px;
        height: 40px;
        font-size: 16px;
        background-color: #4CAF50;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
      }
      
      .resultados {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h1>CONVERTIR MILÍMETROS A METROS, DECÍMETROS, CENTÍMETROS Y MILÍMETROS</h1>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="milimetros">Ingrese la cantidad de milímetros:</label>
        <input type="text" name="milimetros" id="milimetros"><br><br>
        <input type="submit" value="Convertir">
      </form>
      <?php
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Procesar el valor ingresado
        $milimetros = $_POST["milimetros"];
        
        // Convertir milímetros a metros, decímetros, centímetros y milímetros
        $metros = floor($milimetros / 1000);
        $decimetros = floor(($milimetros % 1000) / 100);
        $centimetros = floor(($milimetros % 100) / 10);
        $milimetros_resto = $milimetros % 10;
        
        // Mostrar el resultado
        echo "<div class='resultados'>";
        echo "<h2>Resultados:</h2>";
        echo $milimetros . " milímetros equivalen a " . $metros . " metros, " . $decimetros . " decímetros, " . $centimetros . " centímetros y " . $milimetros_resto . " milímetros.";
        echo "</div>";
      }
      ?>
    </div>
  </body>
</html>

