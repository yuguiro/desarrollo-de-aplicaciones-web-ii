<!DOCTYPE html>
<html>
  <head>
    <title>Números enteros incluidos</title>
    <style>
      body {
        background-color: #808080;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
      }
      
      .container {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
      }
      
      h1 {
        margin-top: 0;
      }
      
      form {
        margin-bottom: 20px;
      }
      
      input[type="submit"] {
        width: 200px;
        height: 40px;
        font-size: 16px;
      }
      
      .resultados {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h1>NÚMEROS ENTEROS INCLUIDOS</h1>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="a">Ingrese el valor de a:</label>
        <input type="text" name="a" id="a"><br><br>
        <label for="b">Ingrese el valor de b:</label>
        <input type="text" name="b" id="b"><br><br>
        <input type="submit" value="Calcular">
      </form>
      <?php
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $a = $_POST["a"];
        $b = $_POST["b"];
        
        $inicio = min($a, $b);
        $fin = max($a, $b);
        $cantidad = $fin - $inicio + 1;
        
        echo "<div class='resultados'>";
        echo "<h2>Resultados:</h2>";
        echo "Entre " . $a . " y " . $b . " hay " . $cantidad . " números enteros incluidos.";
        echo "</div>";
      }
      ?>
    </div>
  </body>
</html>

