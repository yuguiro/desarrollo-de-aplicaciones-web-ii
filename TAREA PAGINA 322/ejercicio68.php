<?php
if (isset($_POST["btnContar"])) {
    $frase = $_POST["txtFrase"];
    $palabras = array();
    $frase = strtolower(preg_replace('/[^\p{L}\p{N}\s]/u', '', $frase));
    $palabras = preg_split('/\s+/', $frase, -1, PREG_SPLIT_NO_EMPTY);

    $repetidas = array();
    $conteo = array();

    foreach ($palabras as $palabra) {
        if (array_key_exists($palabra, $conteo)) {
            $conteo[$palabra]++;
        } else {
            $conteo[$palabra] = 1;
        }
    }

    $repetidas = array_filter($conteo, function ($valor) {
        return $valor > 1;
    });

    $totalRepetidas = count($repetidas);
}
?>

<html>
<head>
    <title>Problema 68</title>
    <link rel="stylesheet" href="estilos68.css">
</head>
<body>
    <form method="post" action="ejercicio68.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 68</strong></td>
            </tr>
            <tr>
                <td>Ingrese una frase</td>
                <td>
                    <textarea name="txtFrase" rows="4" cols="50"><?= $_POST["txtFrase"] ?? '' ?></textarea>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnContar" type="submit" value="Contar" />
                </td>
            </tr>
            <?php if (isset($_POST["btnContar"])) { ?>
                <tr>
                    <td>Cantidad de palabras repetidas</td>
                    <td><?= $totalRepetidas ?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
