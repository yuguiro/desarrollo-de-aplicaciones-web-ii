<?php
if (isset($_POST["btnVerificar"])) {
    $palabra = $_POST["txtPalabra"];
    $mensaje = '';
    $palabra = strtolower(str_replace(' ', '', $palabra));
    $palabraInvertida = strrev($palabra);

    if ($palabra == $palabraInvertida) {
        $mensaje = "La palabra '$palabra' es un palíndromo.";
    } else {
        $mensaje = "La palabra '$palabra' no es un palíndromo.";
    }
}
?>

<html>
<head>
    <title>Problema 66</title>
    <link rel="stylesheet" type="text/css" href="estilos66.css">
</head>
<body>
    <form method="post" action="ejercicio66.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 66</strong></td>
            </tr>
            <tr>
                <td>Ingrese una palabra</td>
                <td>
                    <input name="txtPalabra" type="text" value="<?= $_POST["txtPalabra"] ?? '' ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnVerificar" type="submit" value="Verificar" />
                </td>
            </tr>
            <?php if (isset($_POST["btnVerificar"])) { ?>
                <tr>
                    <td>Resultado</td>
                    <td><?= $mensaje ?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
