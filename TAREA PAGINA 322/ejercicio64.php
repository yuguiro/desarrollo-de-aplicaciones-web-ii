<?php
if (isset($_POST["btnVerificar"])) {
    $letra = $_POST["txtLetra"];
    $mensaje = '';

    if (ctype_lower($letra)) {
        $mensaje = "La letra '$letra' está en minúscula.";
    } elseif (ctype_upper($letra)) {
        $mensaje = "La letra '$letra' está en mayúscula.";
    } else {
        $mensaje = "El valor ingresado no es una letra válida.";
    }
}
?>

<html>
<head>
    <title>Problema 64</title>
    <link rel="stylesheet" type="text/css" href="estilos62.css">
</head>
<body>
    <form method="post" action="ejercicio64.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 64</strong></td>
            </tr>
            <tr>
                <td>Ingrese una letra</td>
                <td>
                    <input name="txtLetra" type="text" value="<?= $_POST["txtLetra"] ?? '' ?>" maxlength="1" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnVerificar" type="submit" value="Verificar" />
                </td>
            </tr>
            <?php if (isset($_POST["btnVerificar"])) { ?>
                <tr>
                    <td>Resultado</td>
                    <td><?= $mensaje ?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
