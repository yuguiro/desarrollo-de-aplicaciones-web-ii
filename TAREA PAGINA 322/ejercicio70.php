<?php
if (isset($_POST["btnDesencriptar"])) {
    $fraseEncriptada = $_POST["txtFraseEncriptada"];
    $fraseDesencriptada = "";

    // Recorrer cada carácter de la frase encriptada
    for ($i = 0; $i < strlen($fraseEncriptada); $i++) {
        $caracter = $fraseEncriptada[$i];

        // Obtener el valor ASCII del carácter y restar 2
        $ascii = ord($caracter);
        $asciiDesencriptado = $ascii - 2;

        // Convertir el valor ASCII en un carácter desencriptado
        $caracterDesencriptado = chr($asciiDesencriptado);

        // Agregar el carácter desencriptado a la frase desencriptada
        $fraseDesencriptada .= $caracterDesencriptado;
    }
}
?>

<html>
<head>
    <title>Problema 70</title>
    <link rel="stylesheet" href="estilos70.css">
</head>
<body>
    <form method="post" action="ejercicio70.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 70</strong></td>
            </tr>
            <tr>
                <td>Frase encriptada</td>
                <td>
                    <textarea name="txtFraseEncriptada" rows="4" cols="50"><?= $_POST["txtFraseEncriptada"] ?? '' ?></textarea>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnDesencriptar" type="submit" value="Desencriptar" />
                </td>
            </tr>
            <?php if (isset($_POST["btnDesencriptar"])) { ?>
                <tr>
                    <td>Frase desencriptada</td>
                    <td><?= $fraseDesencriptada ?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
