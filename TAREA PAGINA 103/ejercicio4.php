<!DOCTYPE html>
<html>
  <head>
    <title>Calcular el doble o triple de un número</title>
    <style>
      body {
        background-color: #808080;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        font-family: Arial, sans-serif;
      }
      
      .container {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
      }
      
      h1 {
        margin-top: 0;
        color: #333;
      }
      
      form {
        margin-bottom: 20px;
      }
      
      label {
        display: block;
        margin-bottom: 10px;
        color: #333;
      }
      
      input[type="number"]::-webkit-inner-spin-button,
      input[type="number"]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
      }
      
      input[type="number"] {
        -moz-appearance: textfield;
      }
      
      input[type="number"] {
        width: 200px;
        height: 30px;
        font-size: 16px;
        padding: 5px;
        color: #333;
        background-color: #fff;
        border: 1px solid #ccc;
        border-radius: 5px;
      }
      
      input[type="submit"] {
        width: 150px;
        height: 40px;
        font-size: 16px;
        background-color: #4CAF50;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
      }
      
      .resultados {
        background-color: #fff;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
        color: #333;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h1>CALCULAR EL DOBLE O TRIPLE DE UN NÚMERO</h1>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="num">Ingrese un número:</label>
        <input type="number" name="num" id="num" pattern="[0-9]*"><br><br>
        <input type="submit" value="Calcular">
      </form>
      <?php
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $num = $_POST["num"];
        
        if ($num > 0) {
          $resultado = $num * 2;
        } elseif ($num < 0) {
          $resultado = $num * 3;
        } else {
          $resultado = 0;
        }
        
        echo "<div class='resultados'>";
        echo "<h2>Resultado:</h2>";
        echo "El resultado es " . $resultado;
        echo "</div>";
      }
      ?>
    </div>
  </body>
</html>


