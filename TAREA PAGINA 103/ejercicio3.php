<!DOCTYPE html>
<html>
  <head>
    <title>Determinar si dos números son iguales o diferentes</title>
    <style>
      body {
        background-color: #808080;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        font-family: Arial, sans-serif;
      }
      
      .container {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
      }
      
      h1 {
        margin-top: 0;
        color: #333;
      }
      
      form {
        margin-bottom: 20px;
      }
      
      label {
        display: block;
        margin-bottom: 10px;
        color: #333;
      }
      
      input[type="number"]::-webkit-inner-spin-button,
      input[type="number"]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
      }
      
      input[type="number"] {
        -moz-appearance: textfield;
      }
      
      input[type="number"] {
        width: 200px;
        height: 30px;
        font-size: 16px;
        padding: 5px;
        color: #333;
        background-color: #fff;
        border: 1px solid #ccc;
        border-radius: 5px;
      }
      
      input[type="submit"] {
        width: 150px;
        height: 40px;
        font-size: 16px;
        background-color: #4CAF50;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
      }
      
      .resultados {
        background-color: #fff;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
        color: #333;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h1>DETERMINAR SI DOS NÚMEROS SON IGUALES O DIFERENTES</h1>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="num1">Ingrese el primer número:</label>
        <input type="number" name="num1" id="num1"><br><br>
        <label for="num2">Ingrese el segundo número:</label>
        <input type="number" name="num2" id="num2"><br><br>
        <input type="submit" value="Calcular">
      </form>
      <?php
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        
        if ($num1 == $num2) {
          $resultado = "iguales";
        } else {
          $resultado = "diferentes";
        }
        
        echo "<div class='resultados'>";
        echo "<h2>Resultado:</h2>";
        echo "Los números ingresados son " . $resultado;
        echo "</div>";
      }
      ?>
    </div>
  </body>
</html>

