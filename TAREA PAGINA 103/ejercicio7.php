<?php
if (isset($_POST["saldo_anterior"]) && isset($_POST["tipo_movimiento"]) && isset($_POST["monto_transaccion"])) {
    $saldo_anterior = $_POST["saldo_anterior"]; 
    $tipo_movimiento = $_POST["tipo_movimiento"]; 
    $monto_transaccion = $_POST["monto_transaccion"]; 

    if ($tipo_movimiento == "D") { 
        $saldo_actual = $saldo_anterior + $monto_transaccion; 
    } elseif ($tipo_movimiento == "R") { 
        $saldo_actual = $saldo_anterior - $monto_transaccion; 
    }

    echo "El saldo actual es: " . $saldo_actual;
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Saldo Actual</title>
    <style>
        body {
            background-color: #808080;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            font-family: Arial, sans-serif;
        }
        
        .container {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            color: #000;
        }
        
        input[type="number"] {
            width: 200px;
            height: 30px;
            font-size: 16px;
            padding: 5px;
            color: #000;
            -moz-appearance: textfield; /* Remover flechas en Firefox */
        }
        
        input[type="number"]::-webkit-outer-spin-button,
        input[type="number"]::-webkit-inner-spin-button {
            -webkit-appearance: none; /* Remover flechas en Chrome, Safari, Opera */
            margin: 0; /* Espacio entre los elementos de la flecha */
        }
        
        input[type="submit"] {
            width: 150px;
            height: 40px;
            font-size: 16px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
        
        .resultados {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            color: #000;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Calcular Saldo Actual</h1>
        <form method="post" action="ejercicio7.php">
            <label for="saldo_anterior">Saldo anterior:</label>
            <input type="number" name="saldo_anterior" id="saldo_anterior" required>
            <br>
            <label for="tipo_movimiento">Tipo de movimiento:</label>
            <select name="tipo_movimiento" id="tipo_movimiento" required>
                <option value="D">Depósito</option>
                <option value="R">Retiro</option>
            </select>
            <br>
            <label for="monto_transaccion">Monto de la transacción:</label>
            <input type="number" name="monto_transaccion" id="monto_transaccion" required>
            <br>
            <input type="submit" value="Calcular saldo">
        </form>

        <?php
        if (isset($_POST["saldo_anterior"]) && isset($_POST["tipo_movimiento"]) && isset($_POST["monto_transaccion"])) {
            $saldo_anterior = $_POST["saldo_anterior"];
            $tipo_movimiento = $_POST["tipo_movimiento"];
            $monto_transaccion = $_POST["monto_transaccion"];

            if ($tipo_movimiento == "D") {
                $saldo_actual = $saldo_anterior + $monto_transaccion;
            } elseif ($tipo_movimiento == "R") {
                $saldo_actual = $saldo_anterior - $monto_transaccion;
            }

            echo "<div class='resultados'>";
            echo "<h2>Resultado:</h2>";
            echo "<p>El saldo actual es: " . $saldo_actual . "</p>";
            echo "</div>";
        }
        ?>
    </div>
</body>
</html>
