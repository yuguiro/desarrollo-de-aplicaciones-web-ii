<!DOCTYPE html>
<html>
  <head>
    <title>Devolver el número menor entre dos números enteros</title>
    <style>
      body {
        background-color: #808080;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        font-family: Arial, sans-serif;
      }
      
      .container {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
      }
      
      h1 {
        margin-top: 0;
        color: #333;
      }
      
      form {
        margin-bottom: 20px;
      }
      
      label {
        display: block;
        margin-bottom: 10px;
        color: #333;
      }
      
      input[type="number"] {
        width: 200px;
        height: 30px;
        font-size: 16px;
        padding: 5px;
        color: #333;
        appearance: none;
        -moz-appearance: textfield; /* Firefox */
      }
      
      input[type="number"]::-webkit-inner-spin-button,
      input[type="number"]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
      }
      
      input[type="submit"] {
        width: 150px;
        height: 40px;
        font-size: 16px;
        background-color: #4CAF50;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
      }
      
      .resultados {
        background-color: #fff;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
        color: #333;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h1>DEVOLVER EL NÚMERO MENOR ENTRE DOS NÚMEROS ENTEROS</h1>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="num1">Ingrese el primer número:</label>
        <input type="number" name="num1" id="num1" pattern="[0-9]*" inputmode="numeric"><br><br>
        <label for="num2">Ingrese el segundo número:</label>
        <input type="number" name="num2" id="num2" pattern="[0-9]*" inputmode="numeric"><br><br>
        <input type="submit" value="Calcular">
      </form>
      <?php
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        
        if ($num1 < $num2) {
          $menor = $num1;
        } else {
          $menor = $num2;
        }
        
        echo "<div class='resultados'>";
        echo "<h2>Resultado:</h2>";
        echo "El número menor es: " . $menor;
        echo "</div>";
      }
      ?>
    </div>
  </body>
</html>
