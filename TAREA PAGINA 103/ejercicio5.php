<!DOCTYPE html>
<html>
  <head>
    <title>Ordenar tres números</title>
    <style>
      body {
        background-color: #808080;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        font-family: Arial, sans-serif;
      }
      
      .container {
        background-color: #D3D3D3;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
      }
      
      h1 {
        margin-top: 0;
        color: #333;
      }
      
      form {
        margin-bottom: 20px;
      }
      
      label {
        display: block;
        margin-bottom: 10px;
        color: #333;
      }
      
      input[type="number"] {
        width: 200px;
        height: 30px;
        font-size: 16px;
        padding: 5px;
        color: #333;
        background-color: #fff;
        border: 1px solid #ccc;
        border-radius: 5px;
      }
      
      input[type="submit"] {
        width: 150px;
        height: 40px;
        font-size: 16px;
        background-color: #4CAF50;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
      }
      
      h2 {
        margin-top: 20px;
        color: #333;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h1>ORDENAR TRES NÚMEROS EN FORMA ASCENDENTE O DESCENDENTE</h1>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="num1">Ingrese el primer número:</label>
        <input type="number" name="num1" id="num1"><br><br>
        <label for="num2">Ingrese el segundo número:</label>
        <input type="number" name="num2" id="num2"><br><br>
        <label for="num3">Ingrese el tercer número:</label>
        <input type="number" name="num3" id="num3"><br><br>
        <input type="submit" value="Ordenar">
      </form>
      <?php
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        $num3 = $_POST["num3"];
        
        $ascendente = [$num1, $num2, $num3];
        sort($ascendente);
        
        $descendente = [$num1, $num2, $num3];
        rsort($descendente);
        
        echo "<h2>Números ordenados en forma ascendente:</h2>";
        foreach ($ascendente as $num) {
          echo $num . " ";
        }
        
        echo "<h2>Números ordenados en forma descendente:</h2>";
        foreach ($descendente as $num) {
          echo $num . " ";
        }
      }
      ?>
    </div>
  </body>
</html>



