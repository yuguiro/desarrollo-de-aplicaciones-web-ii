<!DOCTYPE html>
<html>
<head>
	<title>Determinar si tres longitudes forman un triángulo</title>
</head>
<body>
	<?php
		$a = 7;
		$b = 5;
		$c = 3;

		if ($a < ($b + $c) && $b < ($a + $c) && $c < ($a + $b)) {
			echo "Las longitudes forman un triángulo";
		} else {
			echo "Las longitudes no forman un triángulo";
		}
	?>
</body>
</html>
