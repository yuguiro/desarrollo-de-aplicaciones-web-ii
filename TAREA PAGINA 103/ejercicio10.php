<!DOCTYPE html>
<html>
<head>
	<title>Tipo de triángulo</title>
	<style>
		body {
			background-color: #808080;
			display: flex;
			justify-content: center;
			align-items: center;
			height: 100vh;
			font-family: Arial, sans-serif;
		}
		
		.container {
			background-color: #D3D3D3;
			padding: 40px;
			border-radius: 5px;
			text-align: center;
			color: #000;
		}
		
		label {
			display: inline-block;
			width: 100px;
			text-align: right;
			margin-right: 10px;
		}
		
		input[type="number"] {
			width: 200px;
			height: 30px;
			font-size: 16px;
			padding: 5px;
			color: #000;
		}
		
		input[type="submit"] {
			width: 250px;
			height: 40px;
			font-size: 16px;
			background-color: #4CAF50;
			color: #fff;
			border: none;
			border-radius: 5px;
			cursor: pointer;
		}
		
		.resultados {
			margin-top: 20px;
			font-size: 18px;
		}
	</style>
</head>
<body>
	<div class="container">
		<h1>TIPO DE TRIÁNGULO</h1>
		<form method="post">
			<label for="lado1">Lado 1:</label>
			<input type="number" name="lado1"><br><br>

			<label for="lado2">Lado 2:</label>
			<input type="number" name="lado2"><br><br>

			<label for="lado3">Lado 3:</label>
			<input type="number" name="lado3"><br><br>

			<input type="submit" value="Determinar tipo de triángulo">
		</form>

		<?php
			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				$lado1 = $_POST['lado1'];
				$lado2 = $_POST['lado2'];
				$lado3 = $_POST['lado3'];

				if ($lado1 == $lado2 && $lado1 == $lado3) {
					echo "<div class='resultados'>Tipo de triángulo: Equilátero</div>";
				} elseif ($lado1 == $lado2 || $lado1 == $lado3 || $lado2 == $lado3) {
					echo "<div class='resultados'>Tipo de triángulo: Isósceles</div>";
				} else {
					echo "<div class='resultados'>Tipo de triángulo: Escaleno</div>";
				}
			}
		?>
	</div>
</body>
</html>
