<?php
if(isset($_POST['submit'])) {
    // Obtener las notas ingresadas
    $nota1 = $_POST['nota1'];
    $nota2 = $_POST['nota2'];
    $nota3 = $_POST['nota3'];
    $nota4 = $_POST['nota4'];

    $notas = array($nota1, $nota2, $nota3, $nota4);

    rsort($notas);

    $promedio = ($notas[0] + $notas[1] + $notas[2]) / 3;

    if($promedio >= 11) {
        $mensaje = "Aprobado";
    } else {
        $mensaje = "Desaprobado";
    }

    $resultado = $promedio . " - " . $mensaje;
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Promedio de Notas</title>
    <style>
        body {
            background-color: #808080;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            font-family: Arial, sans-serif;
        }
        
        .container {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            color: #000;
        }
        
        input[type="number"] {
            width: 200px;
            height: 30px;
            font-size: 16px;
            padding: 5px;
            color: #000;
            -moz-appearance: textfield; /* Remover flechas en Firefox */
        }
        
        input[type="number"]::-webkit-outer-spin-button,
        input[type="number"]::-webkit-inner-spin-button {
            -webkit-appearance: none; /* Remover flechas en Chrome, Safari, Opera */
            margin: 0; /* Espacio entre los elementos de la flecha */
        }
        
        input[type="submit"] {
            width: 150px;
            height: 40px;
            font-size: 16px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
        
        .resultados {
            background-color: #D3D3D3;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
            color: #000;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>PROMEDIO DE NOTAS</h1>
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            Nota 1: <input type="number" name="nota1" required><br>
            Nota 2: <input type="number" name="nota2" required><br>
            Nota 3: <input type="number" name="nota3" required><br>
            Nota 4: <input type="number" name="nota4" required><br>
            <input type="submit" name="submit" value="Calcular">
        </form>
        
        <?php if(isset($resultado)): ?>
        <div>
            <h2>Resultado:</h2>
            <p><?php echo $resultado; ?></p>
        </div>
        <?php endif; ?>
    </div>
</body>
</html>

