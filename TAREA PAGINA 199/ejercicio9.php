<html>
<head>
	<title>MCD de dos números</title>
</head>
<body>
	<h1>MCD de dos números</h1>

	<?php
	$num1 = 84;
	$num2 = 18;
	if ($num2 > $num1) {
		$temp = $num1;
		$num1 = $num2;
		$num2 = $temp;
	}

	$resto = $num1 % $num2;

	while ($resto != 0) {
		$num1 = $num2;
		$num2 = $resto;
		$resto = $num1 % $num2;
	}
	echo "El MCD de $num1 y $num2 es: $num2";
	?>
</body>
</html>
