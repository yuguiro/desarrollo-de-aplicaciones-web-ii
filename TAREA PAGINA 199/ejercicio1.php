<!DOCTYPE html>
<html>
<head>
	<title>Factorial de un número</title>
</head>
<body>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<label>Ingrese un número entero positivo:</label>
		<input type="number" name="numero" required>
		<button type="submit">Calcular factorial</button>
	</form>

	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$numero = $_POST["numero"];

		if ($numero < 0) {
			echo "<p>Ingrese un número entero positivo.</p>";
		} else {
			$factorial = 1;
			for ($i = 1; $i <= $numero; $i++) {
				$factorial *= $i;
			}
			echo "<p>El factorial de $numero es: $factorial</p>";
		}
	}
	?>
</body>
</html>
