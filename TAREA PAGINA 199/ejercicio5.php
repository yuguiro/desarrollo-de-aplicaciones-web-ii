<!DOCTYPE html>
<html>
<head>
	<title>Buscar dígito en un número</title>
</head>
<body>
	<form method="post">
		<label for="numero">Ingrese un número:</label>
		<input type="text" name="numero" id="numero">
		<br>
		<label for="digito">Ingrese el dígito a buscar:</label>
		<input type="text" name="digito" id="digito">
		<br>
		<input type="submit" value="Buscar">
	</form>
	<?php
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$numero = $_POST['numero'];
			$digito = $_POST['digito'];
			$encontrado = false;

			for ($i = 0; $i < strlen($numero); $i++) {
				if ($numero[$i] === $digito) {
					$encontrado = true;
					break;
				}
			}

			if ($encontrado) {
				echo "El dígito $digito está presente en el número $numero";
			} else {
				echo "El dígito $digito no está presente en el número $numero";
			}
		}
	?>
</body>
</html>
