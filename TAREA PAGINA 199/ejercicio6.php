<!DOCTYPE html>
<html>
<head>
	<title>Porcentaje de números pares, impares y neutros</title>
</head>
<body>
	<h1>Porcentaje de números pares, impares y neutros</h1>
	<form method="post">
		<label>Ingrese un número:</label>
		<input type="number" name="numero">
		<input type="submit" value="Calcular">
	</form>
	<?php
		if(isset($_POST['numero'])) {
			$numero = $_POST['numero'];
			$numeros = str_split($numero);
			$total_numeros = count($numeros);
			$num_pares = 0;
			$num_impares = 0;
			$num_neutros = 0;

			foreach ($numeros as $digito) {
				if($digito == 0) {
					$num_neutros++;
				} else if($digito % 2 == 0) {
					$num_pares++;
				} else {
					$num_impares++;
				}
			}

			$porcentaje_pares = round($num_pares / $total_numeros * 100, 2);
			$porcentaje_impares = round($num_impares / $total_numeros * 100, 2);
			$porcentaje_neutros = round($num_neutros / $total_numeros * 100, 2);

			echo "<p>Porcentaje de números pares: $porcentaje_pares%</p>";
			echo "<p>Porcentaje de números impares: $porcentaje_impares%</p>";
			echo "<p>Porcentaje de números neutros: $porcentaje_neutros%</p>";
		}
	?>
</body>
</html>
