<!DOCTYPE html>
<html>
<head>
	<title>Números primos en un rango</title>
</head>
<body>
	<h1>Números primos en un rango</h1>
	<form method="post" action="">
		<label for="inicio">Inicio del rango:</label>
		<input type="number" name="inicio" id="inicio">
		<label for="fin">Fin del rango:</label>
		<input type="number" name="fin" id="fin">
		<input type="submit" value="Calcular">
	</form>
	<?php
		function esPrimo($n) {
			if ($n == 2) {
				return true;
			}
			if ($n < 2 || $n % 2 == 0) {
				return false;
			}
			for ($i = 3; $i <= sqrt($n); $i += 2) {
				if ($n % $i == 0) {
					return false;
				}
			}
			return true;
		}

		if (isset($_POST["inicio"]) && isset($_POST["fin"])) {
			$inicio = intval($_POST["inicio"]);
			$fin = intval($_POST["fin"]);
			if ($inicio >= 0 && $fin >= 0 && $inicio <= $fin) {
				$contador = 0;
				for ($i = $inicio; $i <= $fin; $i++) {
					if (esPrimo($i)) {
						$contador++;
					}
				}
				echo "<p>En el rango [$inicio, $fin] hay $contador números primos.</p>";
			} else {
				echo "<p>Error: los valores ingresados no son válidos.</p>";
			}
		}
	?>
</body>
</html>
