<?php
function calcu($base, $altura) {
    $area = $base * $altura;
    return $area;
}

$base = $_POST["txtbase"] ?? '';
$altura = $_POST["txtaltura"] ?? '';
$area = '';

if (isset($_POST["btnCalcular"])) {
    if (!empty($base) && !empty($altura)) {
        $area = calcu($base, $altura);
    }
}
?>

<html>
<head>
    <title>Problema 76</title>
    <link rel="stylesheet" type="text/css" href="estilos76.css">
</head>
<body>
    <form method="post" action="ejercicio76.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 76</strong></td>
            </tr>
            <tr>
                <td>Base</td>
                <td>
                    <input name="txtbase" type="number" value="<?= $base ?>" />
                </td>
            </tr>
            <tr>
                <td>Altura</td>
                <td>
                    <input name="txtaltura" type="number" value="<?= $altura ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" value="Calcular" />
                </td>
            </tr>
            <?php if (isset($_POST["btnCalcular"])) { ?>
                <tr>
                    <td>Area del rectangulo</td>
                    <td><?= $area ?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
