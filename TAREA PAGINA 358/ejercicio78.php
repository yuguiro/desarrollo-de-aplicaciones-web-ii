<?php
function InvertirNumero($numero) {
    $numeroInvertido = strrev($numero);
    return $numeroInvertido;
}

$numero = $_POST["txtnumero"] ?? '';
$numeroInvertido = '';

if (isset($_POST["btnInvertir"])) {
    if (!empty($numero)) {
        $numeroInvertido = InvertirNumero($numero);
    }
}
?>

<html>
<head>
    <title>Problema 78</title>
    <link rel="stylesheet" type="text/css" href="estilos78.css">
</head>
<body>
    <form method="post" action="ejercicio78.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 78</strong></td>
            </tr>
            <tr>
                <td>Número</td>
                <td>
                    <input name="txtnumero" type="number" value="<? $numero ?>" required/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnInvertir" type="submit" value="Invertir" />
                </td>
            </tr>
            <?php if (isset($_POST["btnInvertir"])) { ?>
                <tr>
                    <td>Número invertido</td>
                    <td><?= $numeroInvertido ?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
