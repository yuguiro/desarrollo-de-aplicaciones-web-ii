<?php
function Promedio($N1, $N2, $N3) {
    if ($N1 >= $N2 && $N1 >= $N3) {
        if ($N2 >= $N3) {
            $Promedio = ($N1 + $N2) / 2;
        } else {
            $Promedio = ($N1 + $N3) / 2;
        }
    } elseif ($N2 >= $N1 && $N2 >= $N3) {
        if ($N1 >= $N3) {
            $Promedio = ($N2 + $N1) / 2;
        } else {
            $Promedio = ($N2 + $N3) / 2;
        }
    } else {
        if ($N1 >= $N2) {
            $Promedio = ($N3 + $N1) / 2;
        } else {
            $Promedio = ($N3 + $N2) / 2;
        }
    }
    
    return $Promedio;
}

$nota1 = $_POST["txtnota1"] ?? '';
$nota2 = $_POST["txtnota2"] ?? '';
$nota3 = $_POST["txtnota3"] ?? '';
$promedio = '';

if (isset($_POST["btnCalcular"])) {
    if (!empty($nota1) && !empty($nota2) && !empty($nota3)) {
        $promedio = Promedio($nota1, $nota2, $nota3);
    }
}
?>

<html>
<head>
    <title>Problema 72</title>
    <link rel="stylesheet" type="text/css" href="estilos72.css">
</head>
<body>
    <form method="post" action="ejercicio72.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 72</strong></td>
            </tr>
            <tr>
                <td>Nota 1</td>
                <td>
                    <input name="txtnota1" type="number" value="<?= $nota1 ?>" required/>
                </td>
            </tr>
            <tr>
                <td>Nota 2</td>
                <td>
                    <input name="txtnota2" type="number" value="<?= $nota2 ?>" required/>
                </td>
            </tr>
            <tr>
                <td>Nota 3</td>
                <td>
                    <input name="txtnota3" type="number" value="<?= $nota3 ?>" required/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" value="Calcular" />
                </td>
            </tr>
            <?php if (isset($_POST["btnCalcular"]) && !empty($promedio)) { ?>
                <tr>
                    <td>Promedio de las dos notas mayores</td>
                    <td><?= $promedio ?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
