<?php
function sumadig($numero, &$sumaPares, &$sumaImpares) {
    $sumaPares = 0;
    $sumaImpares = 0;

    while ($numero > 0) {
        $digito = $numero % 10;

        if ($digito % 2 == 0) {
            $sumaPares += $digito;
        } else {
            $sumaImpares += $digito;
        }

        $numero = intdiv($numero, 10);
    }
}

$numero = $_POST["txtnumero"] ?? '';
$sumaPares = 0;
$sumaImpares = 0;

if (isset($_POST["btnCalcular"])) {
    if (!empty($numero)) {
        sumadig($numero, $sumaPares, $sumaImpares);
    }
}
?>

<html>
<head>
    <title>Problema 74</title>
    <link rel="stylesheet" type="text/css" href="estilos74.css">
</head>
<body>
    <form method="post" action="ejercicio74.php">
        <table>
            <tr>
                <td colspan="2"><strong>Problema 74</strong></td>
            </tr>
            <tr>
                <td>Número</td>
                <td>
                    <input name="txtnumero" type="number" value="<?= $numero ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" value="Calcular" />
                </td>
            </tr>
            <?php if (isset($_POST["btnCalcular"])) { ?>
                <tr>
                    <td>Suma dígitos pares</td>
                    <td><?= $sumaPares ?></td>
                </tr>
                <tr>
                    <td>Suma dígitos impares</td>
                    <td><?= $sumaImpares ?></td>
                </tr>
            <?php } ?>
        </table>
    </form>
</body>
</html>
