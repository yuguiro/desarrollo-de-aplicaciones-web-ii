<!DOCTYPE html>
<html>

<head>
  <title>Obtener números mayores de cada columna y sumar matriz</title>
  <style>
    body {
      background-color: #f1f1f1;
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100vh;
    }

    .container {
      max-width: 600px;
      padding: 20px;
      background-color: #ffffff;
      border: 1px solid #cccccc;
      border-radius: 5px;
      text-align: center;
    }

    .container h1 {
      margin-top: 0;
    }

    table {
      margin: 0 auto;
    }
  </style>
</head>

<body>
  <div class="container">
    <h1>Obtener números mayores de cada columna y sumar matriz</h1>
    <form method="post" action="">
      <table>
        <tr>
          <td><input type="text" name="matriz[0][0]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="text" name="matriz[0][1]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="text" name="matriz[0][2]" inputmode="numeric" pattern="[0-9]*" /></td>
        </tr>
        <tr>
          <td><input type="text" name="matriz[1][0]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="text" name="matriz[1][1]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="text" name="matriz[1][2]" inputmode="numeric" pattern="[0-9]*" /></td>
        </tr>
      </table>
      <br>
      <label for="k">Ingrese el valor K:</label>
      <input type="text" name="k" inputmode="numeric" pattern="[0-9]*" />
      <br><br>
      <input type="submit" value="Mostrar respuesta">
    </form>

      <?php
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $matriz = $_POST["matriz"];
    $k = $_POST["k"];

    // Multiplicar el contenido de la matriz por K
    for ($i = 0; $i < count($matriz); $i++) {
      for ($j = 0; $j < count($matriz[$i]); $j++) {
        $matriz[$i][$j] *= $k;
      }
    }

    // Obtener la suma de los números de la matriz
    $suma = 0;
    for ($i = 0; $i < count($matriz); $i++) {
      for ($j = 0; $j < count($matriz[$i]); $j++) {
        $suma += $matriz[$i][$j];
      }
    }

    // Mostrar los números mayores de cada columna y la suma de la matriz
    echo "</table>";
    echo "<h2>Suma de los números de la matriz:</h2>";
    echo "<p>$suma</p>";
    }
  ?>
</body>
</html>