<!DOCTYPE html>
<html>
<head>
    <title>Contador de números pares e impares</title>
    <script>
        function contarParesImpares() {
            var rangoInicial = parseInt(document.getElementById("rango-inicial").value);
            var rangoFinal = parseInt(document.getElementById("rango-final").value);
            var pares = 0;
            var impares = 0;

            for (var numero = rangoInicial; numero <= rangoFinal; numero++) {
                if (numero % 5 === 0) {
                    continue;
                }

                if (numero % 2 === 0) {
                    pares++;
                } else {
                    impares++;
                }
            }

            document.getElementById("resultado-pares").textContent = "Cantidad de números pares: " + pares;
            document.getElementById("resultado-impares").textContent = "Cantidad de números impares: " + impares;
        }
    </script>
</head>
<body>
    <h1>Contador de números pares e impares</h1>
    <label for="rango-inicial">Rango inicial:</label>
    <input type="number" id="rango-inicial" min="0">
    <br>
    <label for="rango-final">Rango final:</label>
    <input type="number" id="rango-final" min="0">
    <br>
    <button onclick="contarParesImpares()">Contar</button>
    <br>
    <p id="resultado-pares"></p>
    <p id="resultado-impares"></p>
</body>
</html>
