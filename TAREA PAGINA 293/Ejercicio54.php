<!DOCTYPE html>
<html>

<head>
    <title>Ordenar números</title>
    <style>
        body {
            background-color: black;
            color: white;
            font-family: "Tequila Sunrise", cursive;
            text-align: center;
            padding-top: 50px;
        }

        .title {
            background-color: #00bfff;
            color: white;
            padding: 10px;
            border-radius: 4px;
            margin-bottom: 20px;
        }

        form {
            margin-bottom: 20px;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="numeric"],
        select {
            width: 200px;
            padding: 5px;
            border-radius: 4px;
            border: none;
            background-color: #222;
            color: white;
        }

        input[type="submit"] {
            background-color: #00bfff;
            color: white;
            font-size: 18px;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        p {
            background-color: #00bfff;
            color: white;
            padding: 5px;
            border-radius: 4px;
        }
    </style>
    <link href="https://fonts.googleapis.com/css2?family=Tequila+Sunrise&display=swap" rel="stylesheet">
</head>

<body>
    <div class="title">
        <h1>Ordenar números segun la forma que se indique Asendente o Descendente</h1>
    </div>
    <form method="post" action="">
        <?php for ($i = 1; $i <= 5; $i++) : ?>
            <label for="num<?= $i ?>">Número <?= $i ?>:</label>
            <input type="numeric" id="num<?= $i ?>" name="numeros[]" required>
            <br><br>
        <?php endfor; ?>
        <label for="forma">Forma:</label>
        <select id="forma" name="forma">
            <option value="ascendente">Ascendente</option>
            <option value="descendente">Descendente</option>
        </select>
        <br><br>
        <input type="submit" value="Mostrar resultado">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $numeros = $_POST["numeros"];
        $forma = $_POST["forma"];

        if ($forma == "ascendente") {
            sort($numeros);
        } else {
            rsort($numeros);
        }

        echo "<p>El Orden seria: " . implode(", ", $numeros) . "</p>";
    }
    ?>
</body>

</html>
