<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $numero1 = $_POST['numero1'];
    $numero2 = $_POST['numero2'];
    $numero3 = $_POST['numero3'];
    $numero4 = $_POST['numero4'];

    $numeros = [$numero1, $numero2, $numero3, $numero4];

    $mayor = max($numeros);
    $menor = min($numeros);
}
?>
<html>
<head>
    <title>Encontrar número mayor y menor</title>
    <style>
        body {
            background-color: black;
            color: white;
            text-transform: uppercase;
            font-family: "Tequila Sunrise", cursive;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            flex-direction: column;
        }

        form {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 20px;
        }

        input[type="number"] {
            width: 200px;
        }

        input[type="submit"] {
            background-color: #00bfff;
            color: white;
            font-size: 18px;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        .resultado {
            background-color: #00bfff;
            color: white;
            padding: 20px;
            text-align: center;
            border-radius: 4px;
        }
    </style>
    <link href="https://fonts.googleapis.com/css2?family=Tequila+Sunrise&display=swap" rel="stylesheet">
</head>
<body>
    <form method="POST" action="">
        <label>Primer número:</label>
        <input type="number" name="numero1" required>
        <br><br>
        <label>Segundo número:</label>
        <input type="number" name="numero2" required>
        <br><br>
        <label>Tercer número:</label>
        <input type="number" name="numero3" required>
        <br><br>
        <label>Cuarto número:</label>
        <input type="number" name="numero4" required>
        <br><br>
        <input type="submit" value="Calcular">
    </form>

    <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
        <div class="resultado">
            <p>El número mayor es: <?= $mayor ?></p>
            <p>El número menor es: <?= $menor ?></p>
        </div>
    <?php endif; ?>
</body>
</html>
