<!DOCTYPE html>
<html>

<head>
  <title>Obtener números mayores de cada columna</title>
  <style>
    body {
      background-color: #f1f1f1;
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100vh;
    }

    .container {
      max-width: 600px;
      padding: 20px;
      background-color: #ffffff;
      border: 1px solid #cccccc;
      border-radius: 5px;
      text-align: center;
    }

    .container h1 {
      margin-top: 0;
    }

    table {
      margin: 0 auto;
    }
    
    input[name^="matriz"][value="10"],
    input[name^="matriz"][value="20"],
    input[name^="matriz"][value="30"],
    input[name^="matriz"][value="40"] {
      width: 5px; 
      height: 5px; 
      font-weight: bold;
      color: red;
    }
  </style>
</head>

<body>
  <div class="container">
    <h1>Obtener números mayores de cada columna</h1>
    <form method="post" action="">
      <table>
        <tr>
          <td><input type="text" name="matriz[0][0]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="text" name="matriz[0][1]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="text" name="matriz[0][2]" inputmode="numeric" pattern="[0-9]*" /></td>
        </tr>
        <tr>
          <td><input type="text" name="matriz[1][0]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="text" name="matriz[1][1]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="text" name="matriz[1][2]" inputmode="numeric" pattern="[0-9]*" /></td>
        </tr>
        <tr>
          <td><input type="text" name="matriz[2][0]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="text" name="matriz[2][1]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="text" name="matriz[2][2]" inputmode="numeric" pattern="[0-9]*" /></td>
        </tr>
        <tr>
          <td><input type="text" name="matriz[3][0]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="text" name="matriz[3][1]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="text" name="matriz[3][2]" inputmode="numeric" pattern="[0-9]*" /></td>
        </tr>
      </table>
      <br>
      <input type="submit" value="Mostrar respuesta">
    </form>

    <?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $matriz = $_POST["matriz"];

  $mayores = array();
  for ($i = 0; $i < count($matriz[0]); $i++) {
    $mayor = $matriz[0][$i];
    for ($j = 1; $j < count($matriz); $j++) {
      if ($matriz[$j][$i] > $mayor) {
        $mayor = $matriz[$j][$i];
      }
    }
    $mayores[] = $mayor;
  }

  echo "<h2>Los Números mayores de cada columna son:</h2>";
  echo "<table>";
  for ($i = 0; $i < count($mayores); $i++) {
    echo "<tr><td>Columna " . ($i + 1) . ":</td><td>" . $mayores[$i] . "</td></tr>";
  }
  echo "</table>";
}
?>