<!DOCTYPE html>
<html>

<head>
  <title>Suma de filas de una matriz</title>
  <style>
    body {
      background-color: #f1f1f1;
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100vh;
    }

    .container {
      max-width: 400px;
      padding: 20px;
      background-color: #ffffff;
      border: 1px solid #cccccc;
      border-radius: 5px;
      text-align: center;
    }

    .container h1 {
      margin-top: 0;
    }

    table {
      margin: 0 auto;
    }
  </style>
</head>

<body>
  <div class="container">
    <h1>Suma de filas de una matriz</h1>
    <form method="post" action="">
      <table>
        <tr>
          <td><input type="number" name="matriz[0][0]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="number" name="matriz[0][1]" inputmode="numeric" pattern="[0-9]*" /></td>
        </tr>
        <tr>
          <td><input type="number" name="matriz[1][0]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="number" name="matriz[1][1]" inputmode="numeric" pattern="[0-9]*" /></td>
        </tr>
        <tr>
          <td><input type="number" name="matriz[2][0]" inputmode="numeric" pattern="[0-9]*" /></td>
          <td><input type="number" name="matriz[2][1]" inputmode="numeric" pattern="[0-9]*" /></td>
        </tr>
      </table>
      <br>
      <input type="submit" value="Mostrar resultado">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $matriz = array(
        array($_POST["matriz"][0][0], $_POST["matriz"][0][1]),
        array($_POST["matriz"][1][0], $_POST["matriz"][1][1]),
        array($_POST["matriz"][2][0], $_POST["matriz"][2][1])
      );
      $fila1_suma = $matriz[0][0] + $matriz[0][1];
      $fila2_suma = $matriz[1][0] + $matriz[1][1];
      $fila3_suma = $matriz[2][0] + $matriz[2][1];
      echo "<table>";
      echo "<tr><td>La suma de la fila 1 es:</td><td>" . $fila1_suma . "</td></tr>";
      echo "<tr><td>La suma de la fila 2 es:</td><td>" . $fila2_suma . "</td></tr>";
      echo "<tr><td>La suma de la fila 3 es:</td><td>" . $fila3_suma . "</td></tr>";
      echo "</table>";
    }
    ?>

